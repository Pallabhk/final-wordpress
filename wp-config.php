<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'blog_final');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'W.N.e]7~xPXd^WjX{MEA~q/5UBLHJ/LHO(h{Cvtn%c=NxSa=TeVFPnGfWb>u1fv8');
define('SECURE_AUTH_KEY',  'bYbkzh^mi?RZ$.K1r#6_hspk8Ec G`b+RN~Z&$ElHxA[WakQZ 0$vugGVUS=RNHg');
define('LOGGED_IN_KEY',    '6%}mk50kS58c|dXxfgthmDQ}=N>j:As2dA`nnj4j^@ZT[U{/ ,X2!x#?D|v,-Mw}');
define('NONCE_KEY',        '?8d:)O:cE)c7]@Zlq8b4cSl<nh,g?]l-$Nv*:cC?MnvZkptFF^ZzB[ xg@}nTQFG');
define('AUTH_SALT',        '<R{MqLq):#u[zXkqwC/ul]ZOj*rAK^q%fEuw*_#h`qkw@e]N!:Kl!Rea3pA)@=rT');
define('SECURE_AUTH_SALT', 'lxp@^ctjdgK}3yL)%{f?NZa|95/Z*3hX8K%4L(@I.Jz6FPYPowduNl-:,4H/}?wC');
define('LOGGED_IN_SALT',   ',SM0OJHCyJ6?S$FC5=yHuB,Y9dSkB&F#,~P;iS5T+5Hif~NqQ<>FRI.DG*]dy@*P');
define('NONCE_SALT',       ',P$24-x7F>R3LS1Ik]u<5dt#+hHI`wy)+Bcb@(yqHb&&7H2F#_M?N7:Wz([s|Oo6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_Krishna_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
