<?php

    function basic_wordprewss(){
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
    }
add_action('after_setup_theme','basic_wordprewss');

function css_js(){
  wp_enqueue_style('main-css',get_stylesheet_uri(),array('basic-test-style'),'v.25.32');
  wp_enqueue_style('basic-test-style',get_template_directory_uri().'/css/test.css',null,'v1.2.04','all');

  wp_enqueue_script('basic-js',get_template_directory_uri().'/js/javascript.js',null,'v1.2',false);
  wp_enqueue_script('jquery');
}
add_action('wp_enqueue_scripts','css_js');
?>