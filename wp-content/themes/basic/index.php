<?php get_header();?>

<section class="post-area">

       <?php
        if(have_posts()){
           while(have_posts()){
               the_post();?>
               <div class="single-post">
                   <h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
                    <?php the_post_thumbnail();?>
                   <?php the_excerpt();?>

                </hr>
               </div>
          <?php }
        }else{
            echo 'No post here';
        }
       ?>

</section>

<?php get_footer();?>
